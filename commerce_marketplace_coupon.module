<?php
/**
 * @file
 * Commerce Coupon overrides for Commerce Marketplace compatibility.
 */

/**
 * Implements hook_form_FORM_ID_alter() for commerce_checkout_pane_settings_form.
 */
function commerce_marketplace_coupon_form_commerce_checkout_pane_settings_form_alter(&$form, &$form_state) {
  $form['settings']['commerce_coupon_add_reload'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reload checkout page on adding new coupon'),
    '#description' => t('This will disable Ajax callback and force reload of the whole checkout page each time after a new coupon is added. Useful when there are blocks displaying order amount(s), which should be updated after adding the new coupon.'),
    '#default_value' => variable_get('commerce_coupon_add_reload', FALSE),
  );
}

/**
 * Implements hook_commerce_checkout_pane_info_alter().
 *
 * Alters coupon pane checkout_form callback to marketplace-specific one.
 */
function commerce_marketplace_coupon_commerce_checkout_pane_info_alter(&$checkout_panes) {
  if (isset($checkout_panes['commerce_coupon'])) {
    $checkout_panes['commerce_coupon']['callbacks']['checkout_form'] = 'commerce_marketplace_coupon_pane_checkout_form';
  }
}

/*
 * Checkout pane callback: marketplace coupon checkout form.
 *
 * Overrides commerce_coupon_pane_checkout_form() for marketplace orders.
 *
 * This is needed just for changing the coupon redemption callback
 * to marketplace-specific one.
 *
 * @see commerce_coupon_pane_checkout_form()
 */
function commerce_marketplace_coupon_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Allow to replace pane content with ajax calls.
  $pane_form = array(
    '#prefix' => '<div id="commerce-checkout-coupon-ajax-wrapper">',
    '#suffix' => '</div>',
  );

  // Store the payment methods in the form for validation purposes.
  $pane_form['coupon_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Coupon Code'),
    '#description' => t('Enter your coupon code here.'),
  );

  $pane_form['coupon_add'] = array(
    '#type' => 'button',
    '#value' => t('Add coupon'),
    '#name' => 'coupon_add',
    '#limit_validation_errors' => array(),
  );
  if (!variable_get('commerce_coupon_add_reload', FALSE)) {
    $pane_form['coupon_add']['#ajax'] = array(
      'callback' => 'commerce_coupon_add_coupon_callback',
      'wrapper' => 'commerce-checkout-coupon-ajax-wrapper',
    );
  }

  $error = '';
  if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'coupon_add') {
    $code = $form_state['input']['commerce_coupon']['coupon_code'];
    if (!empty($code)) {
      $coupon = commerce_marketplace_coupon_redeem_coupon_code($code, $order, $error);

      if ($coupon) {
        // Clear the field value so that the coupon code does not get
        // resubmitted causing an error when user uses main "Continue to next
        // step" submit.
        $pane_form['coupon_code']['#value'] = '';

        // Reload the order so it is not out of date.
        $order = commerce_order_load($order->order_id);

        // Recalculate discounts.
        commerce_cart_order_refresh($order);
      }
    }
    else {
      $error = t('Please enter a code.');
    }

    if (!$error) {
      // If a coupon was invalidated during the cart refresh (e.g. if its
      // discounts failed their conditions), an error message will have been
      // set.
      $error = &drupal_static('commerce_coupon_error_' . strtolower($code));
    }

    if (!$error) {
      // Allow modules/rules to act when a coupon has been successfully added
      // to the cart.
      rules_invoke_all('commerce_coupon_applied_to_cart', $coupon, $order);
    }
    else {
      drupal_set_message($error, 'error');
    }
  }

  // Extract the View and display keys from the cart contents pane setting.
  $pane_view = variable_get('commerce_coupon_checkout_pane_view', 'order_coupon_list|checkout');
  if ($pane_view != 'none') {
    list($view_id, $display_id) = explode('|', $pane_view);
    if (!empty($view_id) && !empty($display_id) && views_get_view($view_id)) {
      $pane_form['redeemed_coupons'] = array(
        '#type' => 'markup',
        '#markup' => commerce_embed_view($view_id, $display_id, array($order->order_id)),
      );
    }
  }

  // Display any new status messages added by this pane within the pane's area.
  if (drupal_get_messages(NULL, FALSE)) {
    $pane_form['status_messages'] = array(
      '#type' => 'markup',
      '#markup' => theme('status_messages'),
      '#weight' => -1,
    );
  }

  return $pane_form;
}

/**
 * Applies a coupon to marketplace orders and returns success or failure.
 *
 * Overrides commerce_coupon_redeem_coupon_code().
 *
 * @see commerce_coupon_redeem_coupon_code()
 */
function commerce_marketplace_coupon_redeem_coupon_code($code, $order, &$error) {
  $group_orders = commerce_marketplace_order_group_load($order->order_group);
  foreach ($group_orders as $group_order) {
    $coupon = commerce_coupon_redeem_coupon_code($code, $group_order, $error);
    $order_wrapper = entity_metadata_wrapper('commerce_order', $group_order);
    module_invoke_all('commerce_cart_order_refresh', $order_wrapper);
    $order_wrapper->save();
  }
  return $coupon;
}


/**
 * Implements hook_menu_alter().
 *
 * @see commerce_coupon_menu()
 */
function commerce_marketplace_coupon_menu_alter(&$items) {
  if (isset($items['commerce/coupons/order/remove/%commerce_coupon/%commerce_order'])) {
    $items['commerce/coupons/order/remove/%commerce_coupon/%commerce_order']['page callback'] = 'commerce_marketplace_coupon_remove_coupon_from_order_callback';
  }
}

/*
 * Page callback: remove a coupon from an order and redirect back to
 * destination.
 *
 * Overrides commerce_coupon_remove_coupon_from_order_callback() for marketplace
 * orders.
 *
 * @see commerce_coupon_remove_coupon_from_order_callback()
 */
function commerce_marketplace_coupon_remove_coupon_from_order_callback($coupon, $order) {
  if (!isset($_GET['token']) || !drupal_valid_token($_GET['token'], 'commerce_coupon_remove_checkout:' . $coupon->coupon_id . ':' . $order->order_id) || !commerce_checkout_access($order)) {
    return MENU_ACCESS_DENIED;
  }

  $group_orders = commerce_marketplace_order_group_load($order->order_group);
  foreach ($group_orders as $group_order) {
    commerce_coupon_remove_coupon_from_order($group_order, $coupon);
  }

  drupal_set_message(t('Coupon removed from order'));
  drupal_goto();
}

/**
 * Implements hook_form_FORM_ID_alter() for views_form_commerce_cart_form_default.
 *
 * Adds marketplace orders support for coupon form displayed on Shopping Cart
 * page in 'Shopping cart form' view's header or footer.
 *
 * Based on commerce_coupon "Show coupon form on cart page" issue patch:
 * @see https://www.drupal.org/node/1684892#comment-9917582
 *
 * @see commerce_coupon_handler_area_cart_form::views_form()
 */
function commerce_marketplace_coupon_form_views_form_commerce_cart_form_default_alter(&$form, &$form_alter) {
  // Replace submit callback for 'Add coupon' button.
  if (isset($form['coupon_cart_form']['coupon_add']['#submit'])) {
    $delta = array_search('commerce_coupon_handler_area_cart_form_submit', $form['coupon_cart_form']['coupon_add']['#submit'], TRUE);
    if ($delta !== FALSE) {
      $form['coupon_cart_form']['coupon_add']['#submit'][$delta] = 'commerce_marketplace_coupon_handler_area_cart_form_submit';
    }
  }
}

/**
 * Submit: function commerce_coupon_handler_area_cart_form.
 *
 * Overrides commerce_coupon_handler_area_cart_form_submit() for marketplace
 * orders.
 *
 * @see commerce_marketplace_coupon_form_views_form_commerce_cart_form_default_alter()
 * @see commerce_coupon_handler_area_cart_form_submit()
 */
function commerce_marketplace_coupon_handler_area_cart_form_submit($form, $form_state) {
  $group_orders = commerce_marketplace_order_group_load($form_state['order']->order_group);
  foreach ($group_orders as $group_order) {
    // commerce_coupon_handler_area_cart_form_submit() takes order from the
    // form state, so we need to update it there for each order being processed.
    $original_order_id = $form_state['order']->order_id;
    $form_state['order'] = $group_order;

    commerce_coupon_handler_area_cart_form_submit($form, $form_state);

    $order_wrapper = entity_metadata_wrapper('commerce_order', $group_order);
    module_invoke_all('commerce_cart_order_refresh', $order_wrapper);
    $order_wrapper->save();
  }
  $form_state['order'] = $group_orders[$original_order_id];
}

/**
 * Implements hook_commerce_order_insert().
 *
 * Updates the newly created order with coupon(s) already attached
 * to other orders in the order group.
 */
function commerce_marketplace_coupon_commerce_order_insert($order) {
  // At this point the $order->order_group is not set yet,
  // so we need to get it in a different way.
  $current_order_group_id = commerce_marketplace_cart_get_current_order_group_id();

  if (!$current_order_group_id) {
    watchdog('commerce_marketplace_coupon', 'Unable to get current order_group ID for order @order_id', array(
      '@order_id' => $order->order_id,
    ), WATCHDOG_ERROR);
    return;
  }

  // Currently created order is not included yet in the $group_orders array.
  $group_orders = commerce_marketplace_order_group_load($current_order_group_id);
  if (!empty($group_orders)) {
    foreach ($group_orders as $group_order) {
      if (
        $group_order->order_id != $order->order_id
        && !empty($group_order->commerce_coupons)
      ) {
        $group_order_wrapper = entity_metadata_wrapper('commerce_order', $group_order);
        foreach ($group_order_wrapper->commerce_coupons->value() as $coupon) {
          if (!commerce_coupon_order_has_coupon_code($coupon->code, $order)) {
            $error = '';
            commerce_coupon_redeem_coupon_code($coupon->code, $order, $error);

            if (!empty($error)) {
              watchdog('commerce_marketplace_coupon', 'Unable to apply coupon @coupon_code to new order @order_id', array(
                '@coupon_code' => $coupon->code,
                '@order_id' => $order->order_id,
              ), WATCHDOG_ERROR);
            }
          }
        }
      }
    }
  }
}
